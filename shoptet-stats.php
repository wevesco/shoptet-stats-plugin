<?php
/**
 * Plugin Name:       Shoptet Stats
 * Description:       Fetch and store Shoptet statistics. Available via shortcode.
 * Version:           1.0.0
 * Author:            Jakub Kolář
 */

class ShoptetStats {

  const SHOPTET_ENDPOINT_URL = 'https://www.shoptet.cz/action/ShoptetStatisticCounts';

  static function init() {
    if ( ! wp_next_scheduled( 'shoptet_stats_fetch' ) ) {
      wp_schedule_event( time(), 'hourly', 'shoptet_stats_fetch' );
    }
    add_action( 'shoptet_stats_fetch', [ get_called_class(), 'fetch' ] );
    add_shortcode( 'shoptet-stats', [ get_called_class(), 'shortcode' ] );
  }

  static function get( $name ) {
    $stats = get_option( 'shoptet_stats', [] );

    if ( ! isset( $stats[$name] ) ) {
      return '';
    }

    return $stats[$name];
  }

  static function fetch() {

    // Initialize CURL
    $ch = curl_init(self::SHOPTET_ENDPOINT_URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Store the data
    $json = curl_exec($ch);
    curl_close($ch);

    // Decode JSON response
    $response = json_decode($json, true);

    // Persist
    if ( ! is_array( $response ) ) {
      $response = [];
    }
    update_option( 'shoptet_stats', $response );
  }

  static function shortcode( $atts ) {
    $stats = '';

    if ( ! empty( $atts['name'] ) ) {
      $stats = self::get( $atts['name'] );
    }

    return $stats;
  }

}

ShoptetStats::init();